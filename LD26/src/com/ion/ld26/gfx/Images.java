package com.ion.ld26.gfx;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;


public class Images {
	
	public static BufferedImage player;
	public static BufferedImage[] players;
	public static BufferedImage tileSheet;
	public static BufferedImage[] tiles;	
	public static BufferedImage portalSheet;
	public static BufferedImage[] portals;
	public static BufferedImage keySheet;
	public static BufferedImage[] keys;
	public static BufferedImage potatoLordSheet;
	public static BufferedImage[] potatoLords;
	public static BufferedImage potatoManSheet;
	public static BufferedImage[] potatoMan;
	
	public static BufferedImage fontSheet;
	public static BufferedImage[] font;
	
	public static BufferedImage youWon;
	public static BufferedImage gameOver;
	public static BufferedImage madeBy;
	public static BufferedImage web;
	public static BufferedImage restart;
	public static BufferedImage ion;
	
	public static int tileWidth = 32;
	public static void load(){
		try {
			player = ImageIO.read(Images.class.getClassLoader().getResourceAsStream("res/player.png"));
			players = ImageHandler.all(player, 8, 1, 2);
			tileSheet = ImageIO.read(Images.class.getClassLoader().getResourceAsStream("res/tiles.png"));
			tiles = ImageHandler.all(tileSheet, 9, 5, 2);
			portalSheet = ImageIO.read(Images.class.getClassLoader().getResourceAsStream("res/portals.png"));
			portals = ImageHandler.all(portalSheet, 9, 3, 2);
			keySheet = ImageIO.read(Images.class.getClassLoader().getResourceAsStream("res/keys.png"));
			keys = ImageHandler.all(keySheet, 9, 1, 2);
			potatoLordSheet = ImageIO.read(Images.class.getClassLoader().getResourceAsStream("res/potatoLords.png"));
			potatoLords = ImageHandler.all(potatoLordSheet, 8, 1, 2);
			potatoManSheet = ImageIO.read(Images.class.getClassLoader().getResourceAsStream("res/potatoMen.png"));
			potatoMan = ImageHandler.all(potatoManSheet, 8, 1, 2);
			fontSheet = ImageIO.read(Images.class.getClassLoader().getResourceAsStream("res/font.png"));
			font = ImageHandler.all(fontSheet, 27, 2, 1);
			ion = ImageIO.read(Images.class.getClassLoader().getResourceAsStream("res/ion.png"));
		}
		catch(IOException e) {
			e.printStackTrace();			
		}
		
		youWon = DrawString.make("YOU WON", 7, 3, 0xFFFFFF);
		gameOver = DrawString.make("GAME OVER", 9, 3, 0xFFFFFF);
		madeBy = DrawString.make("MADE BY:", 8, 2, 0xFFFFFF);
		web = DrawString.make("IONPROGRAMMING.COM", 18, 1, 0xFFFFFF);
		restart = DrawString.make("ENTER TO RESTART", 16, 1, 0xFFFFFF);
	}
}
