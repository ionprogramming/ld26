package com.ion.ld26.gfx;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.ion.ld26.Game;
import com.ion.ld26.map.Map;

public class HUD {
	
	static int startImg = 29;
	static int startPos = 320;
	static int samVar = 7;
	public static boolean prem = false;
	
	
	
	static boolean show = false;
	static int timeLeft = 0;
	static BufferedImage textImg;
	static int h = 4;
	static int w = 10;
	static int th = 32;
	public static int mess = 0;
	static String[] text; 
	static int[] time;
	
	public static void update(Graphics g){
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0, Game.height, Game.width, 38);
		for(int r = 0; r < Map.playerHealth; r++){
			g.drawImage(Images.tiles[27], 32 * r - (2 * r), Game.height + 4, null);
		}
		
		for(int i = 0; i < 8; i++){
			g.drawImage(Images.tiles[startImg + i], startPos + 32 * i - (12 * i), Game.height + 4, null);
		}
		
		for(int i = 0; i < Map.ranRange; i++){
			g.drawImage(Images.tiles[28], startPos + 32 * i - (12 * i), Game.height + 4, null);
		}
		
		
		if(Map.ranRange == 8){
			g.setColor(Color.black);
			g.fillRect(0, 0, Game.width, Game.height + 38);
			if(prem){
				g.drawImage(Images.gameOver, Game.width/2 - Images.gameOver.getWidth()/2, 20, null);	
				prem = false;
			}
			else{
				g.drawImage(Images.youWon, Game.width/2 - Images.youWon.getWidth()/2, 20, null);
			}
			
			g.drawImage(Images.madeBy, Game.width/2 - Images.madeBy.getWidth()/2, 100, null);
			g.drawImage(Images.ion, Game.width/2 - Images.ion.getWidth()/2, 190, null);
			g.drawImage(Images.web, Game.width/2 - Images.web.getWidth()/2, 300, null);
			g.drawImage(Images.restart, Game.width/2 - Images.restart.getWidth()/2, 500, null);
		}
		
		if(show){
			timeLeft--;
			
			BufferedImage img = new BufferedImage(w * th + th, h * th + th, Images.fontSheet.getType());
			Graphics2D g2 = img.createGraphics();
			g2.drawImage(Images.tiles[37], 0, 0, null);
			g2.drawImage(Images.tiles[38], w * th, 0, null);
			g2.drawImage(Images.tiles[39], 0, h * th, null);
			g2.drawImage(Images.tiles[40], w * th, h * th, null);
			g2.setColor(new Color(128,128,128));
			g2.fillRect(0 + th, 0 + th, w * th - th, h* th - th);
			
			for(int x = 1; x < w; x++){
				g2.drawImage(Images.tiles[44], x * th, 0, null);
			}
			for(int x = 1; x < w; x++){
				g2.drawImage(Images.tiles[41], x * th, h * th, null);
			}
			for(int y = 1; y < h; y++){
				g2.drawImage(Images.tiles[42], 0, y * th, null);	
			}
			for(int y = 1; y < h; y++){
				g2.drawImage(Images.tiles[43], w * th, y * th, null);	
			}
			
			
			g2.drawImage(textImg, th, th, null);
			
			BufferedImage draw = ImageHandler.resizeImage(img, img.getWidth() / 2, img.getHeight() / 2);
			g.drawImage(draw, (Game.width - draw.getWidth())/2, 0, null);
			
			if(timeLeft <= 0){
				show = false;
				if(mess >= text.length){
					text = null;
					time = null;
					mess = 0;
				}
				else{
					storyText(text, time, false);
				}
			}
		}
		
		
	}
	
	public static void storyText(String[] txt, int[] tim, boolean n){
		if(n){
			mess = 0;
		}
		text = txt;
		time = tim;
		
		textImg =  DrawString.make(text[mess], text[mess].length(), 2, 0xFFFFFF);
		w = text[mess].length() + 1;
		h = 2;
		timeLeft = time[mess] * 50;
		show = true;
		mess++;
	}
}
