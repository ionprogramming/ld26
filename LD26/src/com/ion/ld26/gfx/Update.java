package com.ion.ld26.gfx;

import com.ion.ld26.Game;


public class Update {
	public static void update(){
		for(int e = 0; e < Game.entities.size(); e++){
			if((int)(Math.abs(Game.entities.get(e).xpos - Game.entities.get(0).xpos)/32) + (int)(Math.abs(Game.entities.get(e).ypos - Game.entities.get(0).ypos)/32) <= 24){
				Game.entities.get(e).update();
			}
		}
	}
}
