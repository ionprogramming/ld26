package com.ion.ld26.gfx;

import java.awt.Graphics;

import com.ion.ld26.Game;
import com.ion.ld26.entities.Entity;
import com.ion.ld26.map.Map;

public class Render {
	
	public static void update(Graphics g){
		for(int y = -16; y < Map.curMap.length + 16; y++){
			for(int x = -16; x < Map.curMap[0].length + 16; x++){
				if(y < 0 || y > Map.curMap.length || x < 0 || x > Map.curMap[0].length){
					if(x*32 - Entity.x > 0 - Images.tileWidth && x*32 - Entity.x < Game.width && y*32 - Entity.y > 0 - Images.tileWidth && y*32 - Entity.y < Game.width){
						g.drawImage(Images.tiles[Map.grass], x*32 - Entity.x, y*32 - Entity.y, null);
						g.drawImage(Images.tiles[Map.grass + 9], x*32 - Entity.x, y*32 - Entity.y, null);
					}
				}
			}
		}
		for(int y = 0; y < Map.curMap.length; y++){
			for(int x = 0; x < Map.curMap[0].length; x++){
				if(x*32 - Entity.x > 0 - Images.tileWidth && x*32 - Entity.x < Game.width && y*32 - Entity.y > 0 - Images.tileWidth && y*32 - Entity.y < Game.width){
					g.drawImage(Images.tiles[Map.grass], x*32 - Entity.x, y*32 - Entity.y, null);
				}
			}
		}
		for(int n = 0; n < Map.path.size(); n++){
			if(Map.path.get(n)[0]*32 - Entity.x > 0 - Images.tileWidth && Map.path.get(n)[0]*32 - Entity.x < Game.width && Map.path.get(n)[1]*32 - Entity.y > 0 - Images.tileWidth && Map.path.get(n)[1]*32 - Entity.y < Game.width){
				g.drawImage(Images.tiles[Map.path.get(n)[2]], Map.path.get(n)[0]*32 - Entity.x, Map.path.get(n)[1]*32 - Entity.y, null);
			}
		}
		for(int e = Game.entities.size() - 1; e >= 0; e--){
			if(Game.entities.get(e).xpos - Entity.x > 0 - Images.tileWidth && Game.entities.get(e).xpos - Entity.x < Game.width && Game.entities.get(e).ypos - Entity.y > 0 - Images.tileWidth && Game.entities.get(e).ypos - Entity.y < Game.width){
				Game.entities.get(e).draw(g);
			}
		}
		HUD.update(g);
	}
}
