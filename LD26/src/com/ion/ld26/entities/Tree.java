package com.ion.ld26.entities;

import com.ion.ld26.gfx.Images;
import com.ion.ld26.map.Map;

public class Tree extends Entity {
	
	public Tree(int x, int y){
		imgs = Images.tiles;
		currentImage = Map.grass + 9;
		xpos = x;
		ypos = y;
		health = 10;
		type = "tree";
	}
	
	@Override
	public void update(){
		
	}
		
}
