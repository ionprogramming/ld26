package com.ion.ld26.entities;

import com.ion.ld26.Game;
import com.ion.ld26.gfx.Images;
import com.ion.ld26.sfx.SFX;

public class Health extends Entity{
	int col = 0;
	public Health(int x, int y){
		imgs = Images.tiles;
		xpos = x;
		ypos = y;
		health = 10;
		currentImage = 27;
		
	}
	
	@Override
	public void pickup(String key){
		SFX.keyPickup.play();
		Game.entities.get(0).health+= 3;
		if(Game.entities.get(0).health > 10){
			Game.entities.get(0).health = 10;
		}
		destroy();
	} 
	
}
