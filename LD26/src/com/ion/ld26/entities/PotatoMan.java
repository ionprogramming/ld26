package com.ion.ld26.entities;

import java.util.ArrayList;

import com.ion.ld26.Game;
import com.ion.ld26.gfx.Images;
import com.ion.ld26.map.Map;

public class PotatoMan extends Entity {
	
	int timer = 0;
	int pathTimer = 0;
	boolean left = false;
	boolean right = false;
	boolean up = false;
	boolean down = false;
	ArrayList<Integer> path = new ArrayList<Integer>();
	
	public PotatoMan(int x, int y){
		speed = 2;
		damage = 1;
		imgs = Images.potatoMan;
		xpos = x;
		ypos = y;
		health = 10;
		currentImage = 0;
	}
	
	@Override
	public void destroy(){
		int e = Game.entities.indexOf(this);
		Game.entities.remove(e);
		if(Game.entities.get(0).health < Game.entities.get(0).healthRange){
			Game.entities.get(0).health+= 3;
			if(Game.entities.get(0).health > 10){
				Game.entities.get(0).health = 10;
			}
		}
	}
	
	@Override
	public void ai(){
		if(canPunch > 0){
			canPunch--;
		}
		if(pathTimer == 15){
			if(newPath){
				path = Game.astar.path(xpos/32, ypos/32, Game.entities.get(0).xpos/32, Game.entities.get(0).ypos/32);
				newPath = false;
			}
			pathTimer = -1;
			if(path.size() > 0){
				left = false;  
				right = false; 
				up = false;    
				down = false;
				if(path.get(0) == 1){
					right = true;
				}
				else if(path.get(0) == 2){
					down = true;
				}
				else if(path.get(0) == 3){
					left = true;
				}
				else if(path.get(0) == 4){
					up = true;
				}
				path.remove(0);
			}
			else if(Map.random.nextInt(2) == 1){
				left = false;  
				right = false; 
				up = false;    
				down = false;  
				int d = Map.random.nextInt(5);
				if(d == 1){
					right = true;
				}
				else if(d == 2){
					down = true;
				}
				else if(d == 3){
					left = true;
				}
				else if(d == 4){
					up = true;
				}
			}
			
		}
		pathTimer++;
		if(health <= 0){
			destroy();
		}
		if(left){
			currentImage = 6;
			if(l){
				xpos-= speed;
			}
		}
		if(right){
			currentImage = 4;
			if(r){
				xpos+= speed;
			}
		}
		if(up){
			currentImage = 2;
			if(u){
				ypos-= speed;
			}
		}
		if(down){
			currentImage = 0;
			if(d){
				ypos+= speed;
			}
		}
		if(timer >= 20){
			timer = 0;
		}
		r = true;
		l = true;
		u = true;
		d = true;
		for(int n = 0; n < Game.entities.size(); n++){
			if(!Game.entities.get(n).equals(this)){
				check(Game.entities.get(n), false);
			}
		}
		imgs();
	}
	
	public void imgs(){
		timer++;
		if(timer < 10){
			if(left && l){
				currentImage = 6;
			}
			if(right && r){
				currentImage = 4;
			}
			if(up && u){
				currentImage = 2;
			}
			if(down && d){
				currentImage = 0;
			}
		}
		else if(timer < 20){
			if(left && l){
				currentImage = 7;
			}
			if(right && r){
				currentImage = 5;
			}
			if(up && u){
				currentImage = 3;
			}
			if(down && d){
				currentImage = 1;
			}
		}
		else{
			timer = -1;
		}
	}

}

