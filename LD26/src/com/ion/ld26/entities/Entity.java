package com.ion.ld26.entities;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import com.ion.ld26.Game;
import com.ion.ld26.gfx.HUD;
import com.ion.ld26.gfx.Images;
import com.ion.ld26.map.Map;
import com.ion.ld26.sfx.SFX;


public class Entity {
	
	public boolean activated = false;
	public int punch = 0;
	public int canPunch = 0;;
	public int damage = 0;
	public int speed = 4;
	public static int x;
	public static int y;
	public int xpos;
	public int ypos;
	public int health;
	public int healthRange;
	public String type;
	public BufferedImage[] imgs;
	public int currentImage;
	public boolean newPath = false;
	public boolean update = false;
	
	public boolean r = true;
	public boolean l = true;
	public boolean u = true;
	public boolean d = true;
	
	public void draw(Graphics g){
		g.drawImage(imgs[currentImage], xpos - x, ypos - y, null);
		if(punch > 0){
			g.drawImage(Images.tiles[25], xpos - x, ypos - y, null);
			punch--;
		}
	}
	
	public void update(){
		ai();
	}
	
	public void ai(){
		
	}
	
	public void destroy(){
		int e = Game.entities.indexOf(this);
		Game.entities.remove(e);
	}
	
	public void check(Entity ent, boolean isPlayer){
		boolean hit = false;
		boolean nd = true;
		boolean nu = true;
		boolean nr = true;
		boolean nl = true;
		boolean inx = false;
		boolean iny = false;
		if(isPlayer && (ent.getClass() == PotatoMan.class || ent.getClass() == PotatoLord.class)){
			if(Math.abs(xpos - ent.xpos) + Math.abs(ypos - ent.ypos) <= 256){
				ent.newPath = true;
			}
			if(ent.canPunch == 0){
				if(Math.abs(xpos - ent.xpos) + Math.abs(ypos - ent.ypos) <= 64){
					ent.punch();
				}
			}
		}
		if(Math.abs(xpos - ent.xpos) < 32){
			inx = true;
		}
		if(Math.abs(ypos - ent.ypos) < 32){
			iny = true;
		}
		if(inx){
			if(ypos - ent.ypos + 1 > -32 && ypos - ent.ypos + 1 < 0){
				nd = false;
				hit = true;
			}
			if(ypos - ent.ypos - 1 < 32 && ypos - ent.ypos - 1 > 0){
				nu = false;
				hit = true;
			}
		}
		if(iny){
			if(xpos - ent.xpos + 1 > -32 && xpos - ent.xpos + 1 < 0){
				nr = false;
				hit = true;
			}
			if(xpos - ent.xpos - 1 < 32 && xpos - ent.xpos - 1 > 0){
				nl = false;
				hit = true;
			}
		}
		if(isPlayer){
			if(hit && (ent.getClass() == Key.class || ent.getClass() == Health.class)){
				nd = true;
				nu = true;
				nr = true;
				nl = true;
				int d = Math.abs(ent.xpos - xpos) + Math.abs(ent.ypos - ypos);
				if(d < 16){
					ent.pickup(ent.type);
				}
			}
			else if(hit && ent.getClass() == Portal.class){
				nd = true;
				nu = true;
				nr = true;
				nl = true;
				int e = Game.entities.indexOf(ent);
				if(!Game.entities.get(e).activated){
					if(Game.entities.get(e).type == "greyportal" && Map.greyPortal){
						Game.entities.get(e).activatePortal();
					}
					if(Game.entities.get(e).type == "redportal" && Map.redPortal){
						Game.entities.get(e).activatePortal();
					}
					if(Game.entities.get(e).type == "orangeportal" && Map.orangePortal){
						Game.entities.get(e).activatePortal();
					}
					if(Game.entities.get(e).type == "yellowportal" && Map.yellowPortal){
						Game.entities.get(e).activatePortal();
					}
					if(Game.entities.get(e).type == "greenportal" && Map.greenPortal){
						Game.entities.get(e).activatePortal();
					}
					if(Game.entities.get(e).type == "blueportal" && Map.bluePortal){
						Game.entities.get(e).activatePortal();
					}
					if(Game.entities.get(e).type == "indigoportal" && Map.indigoPortal){
						Game.entities.get(e).activatePortal();
					}
					if(Game.entities.get(e).type == "violetportal" && Map.violetPortal){
						Game.entities.get(e).activatePortal();
					}
					if(Game.entities.get(e).type == "rainbowportal" && Map.rainbowPortal){
						Game.entities.get(e).activatePortal();
					}
				}
				else{
					int d = Math.abs(Game.entities.get(e).xpos - xpos) + Math.abs(Game.entities.get(e).ypos - ypos);
					if(d < 8){
						SFX.teleport.play();
						if(Game.entities.get(e).type == "greyportal"){
							Map.initStage(1);
							HUD.storyText(new String[]{"The potatolord's influence", "has been banished from", "your land", "continue your quest!"}, new int[]{2, 2, 2, 2}, true);
						}
						else if(Game.entities.get(e).type == "redportal"){
							Map.initStage(2);
						}
						else if(Game.entities.get(e).type == "orangeportal"){
							Map.initStage(3);
						}
						else if(Game.entities.get(e).type == "yellowportal"){
							Map.initStage(4);
						}
						else if(Game.entities.get(e).type == "greenportal"){
							Map.initStage(5);
						}
						else if(Game.entities.get(e).type == "blueportal"){
							Map.initStage(6);
						}
						else if(Game.entities.get(e).type == "indigoportal"){
							Map.initStage(7);
						}
						else if(Game.entities.get(e).type == "violetportal"){
							Map.initStage(8);
						}
						else if(Game.entities.get(e).type == "rainbowportal"){
							Map.initStage(9);
						}
					}
				}
			}
		}
		if(hit && punch == 1 && (ent.getClass() == PotatoLord.class || ent.getClass() == PotatoMan.class || ent.getClass() == Player.class)){
			if(getClass() == PotatoLord.class){
				ent.hurt(damage);
				System.out.println(damage);
			}
			else if(!nd && (currentImage == 0 || currentImage == 1)){
				ent.hurt(damage);
			}
			else if(!nu && (currentImage == 2 || currentImage == 3)){
				ent.hurt(damage);
			}
			else if(!nl && (currentImage == 6 || currentImage == 7)){
				ent.hurt(damage);
			}
			else if(!nr && (currentImage == 4 || currentImage == 5)){
				ent.hurt(damage);
			}
		}
		if(!nd){
			d = false;
		}
		if(!nu){
			u = false;
		}
		if(!nl){
			l = false;
		}
		if(!nr){
			r = false;
		}
	}
	public void pickup(String key){
		
	}
	public void activatePortal(){
		
	}
	public void punch(){
		punch = 5;
		canPunch = 25;
	}
	public void hurt(int strength){
		health-= strength;
		SFX.hurt.play();
	}
}
