package com.ion.ld26.entities;

import com.ion.ld26.gfx.Images;
import com.ion.ld26.sfx.SFX;


public class Portal extends Entity{
	int portcol = 0;
	int timer = 0;
	int currentImg = 0;
	
	public Portal(int x, int y, String colour){
		imgs = Images.portals;
		xpos = x;
		ypos = y;
		health = 10;
		if(colour == "grey"){
			portcol = 0;
			type = "greyportal";
			activated = true;
		}
		if(colour == "red"){
			portcol = 3;
			type = "redportal";
		}
		if(colour == "orange"){
			portcol = 6;
			type = "orangeportal";
		}
		if(colour == "yellow"){
			portcol = 9;
			type = "yellowportal";
		}
		if(colour == "green"){
			portcol = 12;
			type = "greenportal";
		}
		if(colour == "blue"){
			portcol = 15;
			type = "blueportal";
		}
		if(colour == "indigo"){
			portcol = 18;
			type = "indigoportal";
		}
		if(colour == "violet"){
			portcol = 21;
			type = "violetportal";
		}
		if(colour == "rainbow"){
			portcol = 24;
			type = "rainbowportal";
		}
		currentImage = portcol;
		
	}
	
	@Override
	public void activatePortal(){
		activated = true;
		SFX.portal.play();
	}
	
	@Override
	public void ai(){
		if(activated){
			timer++;
			if(timer > 2){
				timer = 0;
				currentImg++;
				if(currentImg > 2){
					currentImg = 0;
				}
				currentImage = currentImg + portcol;
				
			}
		}
	}

}
