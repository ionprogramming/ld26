package com.ion.ld26.entities;

import com.ion.ld26.Game;
import com.ion.ld26.gfx.HUD;
import com.ion.ld26.gfx.Images;
import com.ion.ld26.map.Map;
import com.ion.ld26.sfx.SFX;

public class PotatoLord extends Entity {
	int startImg = 1;
	String col;
	public PotatoLord(int x, int y, String colour){
		imgs = Images.potatoLords;
		xpos = x;
		ypos = y;
		health = 10;
		col = colour;
		damage = 2;
		if(colour == "red"){
			startImg = 0;
			type = "redpotatolord";
		}
		if(colour == "orange"){
			startImg = 1;
			type = "orangepotatolord";
		}
		if(colour == "yellow"){
			startImg = 2;
			type = "yellowpotatolord";
		}
		if(colour == "green"){
			startImg = 3;
			type = "greenpotatolord";
		}
		if(colour == "blue"){
			startImg = 4;
			type = "bluepotatolord";
		}
		if(colour == "indigo"){
			startImg = 5;
			type = "indigopotatolord";
			damage = 3;
		}
		if(colour == "violet"){
			startImg = 6;
			type = "violetpotatolord";
			damage = 3;
		}
		if(colour == "rainbow"){
			startImg = 7;
			type = "rainbowpotatolord";
			damage = 4;
		}
		currentImage = startImg;
	}
	@Override
	public void destroy(){
		SFX.explosion.play();
		int e = Game.entities.indexOf(this);
		Game.entities.remove(e);
		Game.entities.add(new Portal(xpos - 2 * 32, ypos - 2 * 32, "grey"));
		HUD.storyText(new String[]{"You defeated the potatolord!", "Collect his key then head home", "through the grey portal"}, new int[]{2, 2, 2}, true);
		if(col == "red"){
			Game.entities.add(new Key(xpos, ypos, "orange"));
			if(Map.ranRange == 0){
				Map.ranRange++;
			}
		}
		if(col == "orange"){
			Game.entities.add(new Key(xpos, ypos, "yellow"));
			if(Map.ranRange == 1){
				Map.ranRange++;
			}
		}
		if(col == "yellow"){
			Game.entities.add(new Key(xpos, ypos, "green"));
			if(Map.ranRange == 2){
				Map.ranRange++;
			}
		}
		if(col == "green"){
			Game.entities.add(new Key(xpos, ypos, "blue"));
			if(Map.ranRange == 3){
				Map.ranRange++;
			}
		}
		if(col == "blue"){
			Game.entities.add(new Key(xpos, ypos, "indigo"));
			if(Map.ranRange == 4){
				Map.ranRange++;
			}
		}
		if(col == "indigo"){
			Game.entities.add(new Key(xpos, ypos, "violet"));
			if(Map.ranRange == 5){
				Map.ranRange++;
			}
		}
		if(col == "violet"){
			Game.entities.add(new Key(xpos, ypos, "rainbow"));
			if(Map.ranRange == 6){
				Map.ranRange++;
				Map.lim++;
			}
		}
		if(col == "rainbow"){
			if(Map.ranRange == 7){
				Map.ranRange++;
				Map.lim++;
			}
		}
	}
	
	@Override
	public void ai(){
		if(canPunch > 0){
			canPunch--;
		}
		if(health <= 0){
			destroy();
		}
		r = true;
		l = true;
		u = true;
		d = true;
		for(int n = 0; n < Game.entities.size(); n++){
			if(!Game.entities.get(n).equals(this)){
				check(Game.entities.get(n), false);
			}
		}
	}
}
