package com.ion.ld26.entities;

import com.ion.ld26.Game;
import com.ion.ld26.gfx.HUD;
import com.ion.ld26.gfx.Images;
import com.ion.ld26.input.Keys;
import com.ion.ld26.map.Map;
import com.ion.ld26.sfx.SFX;

public class Player extends Entity{

	int timer = 0;
	
	public Player(int x, int y){
		health = Map.playerHealth;
		imgs = Images.players;
		currentImage = 2;
		xpos = x;
		Entity.x = x - Game.width/2 + imgs[0].getWidth()/2;
		ypos = y;
		Entity.y = y - Game.height/2 + imgs[0].getHeight()/2;
		healthRange = 10;
		type = "player";
		damage = 2;
	}
	
	@Override
	public void ai(){
		Map.playerHealth = health;
		if(Map.playerHealth <= 0){
			Map.ranRange = 8;
			HUD.prem = true;
		}
		else{
			if(Keys.left){
				currentImage = 6;
				if(l){
					xpos-= speed;
					x-= speed;
				}
			}
			if(Keys.right){
				currentImage = 4;
				if(r){
					xpos+= speed;
					x+= speed;
				}
			}
			if(Keys.up){
				currentImage = 2;
				if(u){
					ypos-= speed;
					y-= speed;
				}
			}
			if(Keys.down){
				currentImage = 0;
				if(d){
					ypos+= speed;
					y+= speed;
				}
			}
			if(timer >= 20){
				timer = 0;
			}
			imgs();
			r = true;
			l = true;
			u = true;
			d = true;
			for(int n = 0; n < Game.entities.size(); n++){
				if(!Game.entities.get(n).equals(this)){
					check(Game.entities.get(n), true);
				}
			}
		}
	}
	
	public void imgs(){
		timer++;
		if(timer < 10){
			if(Keys.left && l){
				currentImage = 6;
			}
			if(Keys.right && r){
				currentImage = 4;
			}
			if(Keys.up && u){
				currentImage = 2;
			}
			if(Keys.down && d){
				currentImage = 0;
			}
		}
		else if(timer < 20){
			if(Keys.left && l){
				currentImage = 7;
			}
			if(Keys.right && r){
				currentImage = 5;
			}
			if(Keys.up && u){
				currentImage = 3;
			}
			if(Keys.down && d){
				currentImage = 1;
			}
		}
		else{
			timer = -1;
		}
	}
	
	@Override
	public void pickup(String key){
		if(key == "greykey"){
			Map.greyPortal = true;
		}
		if(key == "redkey"){
			Map.redPortal = true;
			HUD.storyText(new String[]{"Oh no!", "the colours invaded your land!", "You have collected a red key", "Use it to open the red portal"}, new int[]{2, 2, 2, 2}, true);
		}
		if(key == "orangekey"){
			Map.orangePortal = true;
		}
		if(key == "yellowkey"){
			Map.yellowPortal = true;
		}
		if(key == "greenkey"){
			Map.greenPortal = true;
		}
		if(key == "bluekey"){
			Map.bluePortal = true;
		}
		if(key == "indigokey"){
			Map.indigoPortal = true;
		}
		if(key == "violetkey"){
			Map.violetPortal = true;
		}
		if(key == "rainbowkey"){
			Map.rainbowPortal = true;
		}
	}
	
	@Override
	public void punch(){
		SFX.punch.play();
		punch = 5;
	}
	@Override
	public void hurt(int strength){
		if(health > 0){
			health-= strength;
		}
		SFX.hurt.play();
	}
}
