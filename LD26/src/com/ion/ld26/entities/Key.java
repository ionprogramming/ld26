package com.ion.ld26.entities;

import com.ion.ld26.Game;
import com.ion.ld26.gfx.Images;
import com.ion.ld26.map.Map;
import com.ion.ld26.sfx.SFX;

public class Key extends Entity{
	int col = 0;
	public Key(int x, int y, String colour){
		imgs = Images.keys;
		xpos = x;
		ypos = y;
		health = 10;
		
			
		if(colour == "gray"){
			col = 0;
			type = "graykey";
		}
		if(colour == "red"){
			col = 1;
			type = "redkey";
		}
		if(colour == "orange"){
			col = 2;
			type = "orangekey";
		}
		if(colour == "yellow"){
			col = 3;
			type = "yellowkey";
		}
		if(colour == "green"){
			col = 4;
			type = "greenkey";
		}
		if(colour == "blue"){
			col = 5;
			type = "bluekey";
		}
		if(colour == "indigo"){
			col = 6;
			type = "indigokey";
		}
		if(colour == "violet"){
			col = 7;
			type = "violetkey";
		}
		if(colour == "rainbow"){
			col = 8;
			type = "rainbowkey";
		}
		currentImage = col;
		
	}
	
	@Override
	public void pickup(String key){
		SFX.keyPickup.play();
		Game.entities.get(0).pickup(key);
		destroy();
		if(key == "redkey"){
			Map.initStage(1);
		}
	} 
	
}
