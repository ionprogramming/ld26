package com.ion.ld26;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import com.ion.ld26.astar.Astar;
import com.ion.ld26.entities.Entity;
import com.ion.ld26.gfx.Images;
import com.ion.ld26.gfx.Render;
import com.ion.ld26.gfx.Update;
import com.ion.ld26.input.Keys;
import com.ion.ld26.map.Map;
import com.ion.ld26.sfx.SFX;


public class Game extends Applet implements Runnable, KeyListener{
		
		private static final long serialVersionUID = 1L;
		
		private Image dbImage;
		private Graphics dbg;
	
		public static int width = 512;
		public static int height = 512;
		
		public static ArrayList<Entity> entities = new ArrayList<Entity>();
		public static Astar astar;
		public void init(){
			astar = new Astar();
			astar.start();
			addKeyListener(this);
			setSize(width, height+38);
			setFocusable(true);
			setBackground(Color.black);
			SFX.portal = getAudioClip(getCodeBase(), "res/sounds/activatePortal.au");
			SFX.keyPickup = getAudioClip(getCodeBase(), "res/sounds/keyPickup.au");
			SFX.punch = getAudioClip(getCodeBase(), "res/sounds/punch.au");
			SFX.teleport = getAudioClip(getCodeBase(), "res/sounds/teleport.au");
			SFX.hurt = getAudioClip(getCodeBase(), "res/sounds/hurt.au");
			SFX.explosion = getAudioClip(getCodeBase(), "res/sounds/explosion.au");
			SFX.music = getAudioClip(getCodeBase(), "res/sounds/music.au");
			Images.load();
			Map.initStage(0);
			SFX.mute = true;
		}
		
		
		public void start(){
			Thread th = new Thread(this);
			th.start();
			SFX.music.loop();
		}
		
		public void run(){
			Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
			while(true){
				repaint();
						
				try{
					Thread.sleep(20);
				} 
				catch(InterruptedException ex){
					ex.printStackTrace();
				}
				Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
			}
		}
		
		public void paint(Graphics g){
			Render.update(g);
		}
		
		public void update (Graphics g){
			if (dbImage == null){
		        dbImage = createImage (this.getSize().width, this.getSize().height);
		        dbg = dbImage.getGraphics ();
		    }
		    dbg.setColor (getBackground ());
		    dbg.fillRect (0, 0, this.getSize().width, this.getSize().height);
		    dbg.setColor (getForeground());	    
		    paint (dbg);
		    g.drawImage (dbImage, 0, 0, this);
		    Update.update();
		}


		@Override
		public void keyPressed(KeyEvent e) {
			Keys.keyPressed(e);			
		}


		@Override
		public void keyReleased(KeyEvent e) {
			Keys.keyReleased(e);		
		}


		@Override
		public void keyTyped(KeyEvent e) {
			Keys.keyTyped(e);
		}
		
		public void stop(){
			SFX.music.stop();
			astar.stop = true;	
		}
		public void destroy(){
			SFX.music.stop();
		}
}
		
