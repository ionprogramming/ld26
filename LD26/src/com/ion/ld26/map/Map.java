package com.ion.ld26.map;

import java.util.ArrayList;
import java.util.Random;

import com.ion.ld26.Game;
import com.ion.ld26.entities.Health;
import com.ion.ld26.entities.Key;
import com.ion.ld26.entities.Player;
import com.ion.ld26.entities.Portal;
import com.ion.ld26.entities.PotatoLord;
import com.ion.ld26.entities.PotatoMan;
import com.ion.ld26.entities.Tree;
import com.ion.ld26.gfx.HUD;

public class Map {
	
	public static boolean needRegen = false;
	public static Random random = new Random();
	public static boolean randomize = false;
	public static int ranRange = 0;
	public static int lim = 7;
	public static int trees = 10000;
	public static int men = 500;
	public static int h = 50;
	public static int grass = 8;
	public static ArrayList<int[]> path = new ArrayList<int[]>();
	public static int[][] curMap;
	public static int[][] home0 = new int[][]{{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
	public static int[][] home1 = new int[][]{{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 7, 1, 1, 1, 1, 1, 1, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
	
	public static int playerHealth = 10;
	
	public static boolean greyPortal = false;
	public static boolean redPortal = false;
	public static boolean orangePortal = false;
	public static boolean yellowPortal = false;
	public static boolean greenPortal = false;
	public static boolean bluePortal = false;
	public static boolean indigoPortal = false;
	public static boolean violetPortal = false;
	public static boolean rainbowPortal = false;
	
	public static void addThings(boolean world){
		for(int y = 0; y < curMap.length; y++){
			for(int x = 0; x < curMap[0].length; x++){
				if(curMap[y][x] == 1){
					if(randomize){
						grass = random.nextInt(lim - ranRange) + ranRange;
					}
					Game.entities.add(new Tree(x*32, y*32));
				}
				if(curMap[y][x] == 2){
					Game.entities.add(new Portal(x*32, y*32, "red"));
				}
				if(curMap[y][x] == 3){
					Game.entities.add(new Portal(x*32, y*32, "orange"));
				}
				if(curMap[y][x] == 4){
					Game.entities.add(new Portal(x*32, y*32, "yellow"));
				}
				if(curMap[y][x] == 5){
					Game.entities.add(new Portal(x*32, y*32, "green"));
				}
				if(curMap[y][x] == 6){
					Game.entities.add(new Portal(x*32, y*32, "blue"));
				}
				if(curMap[y][x] == 7){
					Game.entities.add(new Portal(x*32, y*32, "indigo"));
				}
				if(curMap[y][x] == 8){
					Game.entities.add(new Portal(x*32, y*32, "violet"));
				}
				if(curMap[y][x] == 9){
					Game.entities.add(new Portal(x*32, y*32, "rainbow"));
				}
			}
		}	
		if(world){
			path = genPath(1, 1, 240, 240);
			for(int n = 0; n < men; n++){
				int y = random.nextInt(250) + 4;
				int x = random.nextInt(250) + 4;
				if(curMap[y][x] != 1){
					Game.entities.add(new PotatoMan(x*32, y*32));
				}
			}
			for(int n = 0; n < h; n++){
				int y = random.nextInt(250) + 4;
				int x = random.nextInt(250) + 4;
				if(curMap[y][x] != 1){
					Game.entities.add(new Health(x*32, y*32));
				}
			}
		}
	}
	
	
	public static void initStage(int stage){
		Game.entities.clear();
		path.clear();
		if(stage == 0){
			greyPortal = false;    
			redPortal = false;     
			orangePortal = false;  
			yellowPortal = false;  
			greenPortal = false;   
			bluePortal = false;    
			indigoPortal = false;  
			violetPortal = false;  
			rainbowPortal = false; 
			playerHealth = 10;
			grass = 8;
			lim = 7;
			curMap = home0;
			Game.entities.add(new Player(512, 576));
			addThings(false);
			Game.entities.add(new Key(512, 512, "red"));
		}
		else if(stage == 1){
			curMap = home1;
			randomize = true;
			Game.entities.add(new Player(512, 512));
			addThings(false);
			randomize = false;
		}
		else if(stage == 2){
			grass = 0;
			needRegen = true;
			while(needRegen){
				Game.entities.clear();
				needRegen = false;
				curMap = new int[256][256];
				for(int n = 0; n < trees; n++){
					int x = random.nextInt(256);
					int y = random.nextInt(256);
					if((x != 1 || y != 1) && (x != 240 || y != 240) && (x != 238 || y != 238)){
						curMap[y][x] = 1;
					}
				}
				borderise();
				Game.entities.add(new Player(32, 32));
				Game.entities.add(new PotatoLord(7680, 7680, "red"));
				addThings(true);
			}
			HUD.storyText(new String[]{"You must defeat...", "the red potatolord!",  "follow the path to find him", "watch out for his minions!", "kill them with the spacebar", "to gain health"}, new int[]{2, 2, 2, 2, 2, 2}, true);
		}
		else if(stage == 3){
			grass = 1;
			needRegen = true;
			while(needRegen){
				needRegen = false;
				curMap = new int[256][256];
				for(int n = 0; n < trees; n++){
					int x = random.nextInt(256);
					int y = random.nextInt(256);
					if((x != 1 || y != 1) && (x != 240 || y != 240) && (x != 238 || y != 238)){
						curMap[y][x] = 1;
					}
				}
				borderise();
				Game.entities.add(new Player(32, 32));
				Game.entities.add(new PotatoLord(7680, 7680, "orange"));
				addThings(true);
			}
			HUD.storyText(new String[]{"You must now defeat...", "the orange potatolord!"}, new int[]{2, 2}, true);
		}
		else if(stage == 4){
			grass = 2;
			needRegen = true;
			while(needRegen){
				needRegen = false;
				curMap = new int[256][256];
				for(int n = 0; n < trees; n++){
					int x = random.nextInt(256);
					int y = random.nextInt(256);
					if((x != 1 || y != 1) && (x != 240 || y != 240) && (x != 238 || y != 238)){
						curMap[y][x] = 1;
					}
				}
				borderise();
				Game.entities.add(new Player(32, 32));
				Game.entities.add(new PotatoLord(7680, 7680, "yellow"));
				addThings(true);
				HUD.storyText(new String[]{"You must now defeat...", "the yellow potatolord!"}, new int[]{2, 2}, true);
			}
		}
		else if(stage == 5){
			grass = 3;
			needRegen = true;
			while(needRegen){
				needRegen = false;
				curMap = new int[256][256];
				for(int n = 0; n < trees; n++){
					int x = random.nextInt(256);
					int y = random.nextInt(256);
					if((x != 1 || y != 1) && (x != 240 || y != 240) && (x != 238 || y != 238)){
						curMap[y][x] = 1;
					}
				}
				borderise();
				Game.entities.add(new Player(32, 32));
				Game.entities.add(new PotatoLord(7680, 7680, "green"));
				addThings(true);
				HUD.storyText(new String[]{"You must now defeat...", "the green potatolord!"}, new int[]{2, 2}, true);
			}
		}
		else if(stage == 6){
			grass = 4;
			needRegen = true;
			while(needRegen){
				needRegen = false;
				curMap = new int[256][256];
				for(int n = 0; n < trees; n++){
					int x = random.nextInt(256);
					int y = random.nextInt(256);
					if((x != 1 || y != 1) && (x != 240 || y != 240) && (x != 238 || y != 238)){
						curMap[y][x] = 1;
					}
				}
				borderise();
				Game.entities.add(new Player(32, 32));
				Game.entities.add(new PotatoLord(7680, 7680, "blue"));
				addThings(true);
				HUD.storyText(new String[]{"You must now defeat...", "the blue potatolord!"}, new int[]{2, 2}, true);
			}
		}
		else if(stage == 7){
			grass = 5;
			needRegen = true;
			while(needRegen){
				needRegen = false;
				curMap = new int[256][256];
				for(int n = 0; n < trees; n++){
					int x = random.nextInt(256);
					int y = random.nextInt(256);
					if((x != 1 || y != 1) && (x != 240 || y != 240) && (x != 238 || y != 238)){
						curMap[y][x] = 1;
					}
				}
				borderise();
				Game.entities.add(new Player(32, 32));
				Game.entities.add(new PotatoLord(7680, 7680, "indigo"));
				addThings(true);
				HUD.storyText(new String[]{"You must now defeat...", "the indigo potatolord!"}, new int[]{2, 2}, true);
			}
		}
		else if(stage == 8){
			grass = 6;
			needRegen = true;
			while(needRegen){
				needRegen = false;
				curMap = new int[256][256];
				for(int n = 0; n < trees; n++){
					int x = random.nextInt(256);
					int y = random.nextInt(256);
					if((x != 1 || y != 1) && (x != 240 || y != 240) && (x != 238 || y != 238)){
						curMap[y][x] = 1;
					}
				}
				borderise();
				Game.entities.add(new Player(32, 32));
				Game.entities.add(new PotatoLord(7680, 7680, "violet"));
				addThings(true);
				HUD.storyText(new String[]{"You must now defeat...", "the violet potatolord!"}, new int[]{2, 2}, true);
			}
		}
		else if(stage == 9){
			grass = 7;
			needRegen = true;
			while(needRegen){
				needRegen = false;
				curMap = new int[256][256];
				for(int n = 0; n < trees; n++){
					int x = random.nextInt(256);
					int y = random.nextInt(256);
					if((x != 1 || y != 1) && (x != 240 || y != 240) && (x != 238 || y != 238)){
						curMap[y][x] = 1;
					}
				}
				borderise();
				Game.entities.add(new Player(32, 32));
				Game.entities.add(new PotatoLord(7680, 7680, "rainbow"));
				addThings(true);
				HUD.storyText(new String[]{"the potatolords have", "combined their hues to become", "the rainbow potatolord!", "careful... he bites!"}, new int[]{2, 2, 2, 2}, true);
			}
		}
	}
	
	public static void borderise(){
		for(int y = 0; y < curMap.length; y++){
			curMap[y][0] = 1;
			curMap[y][curMap.length - 1] = 1;
		}
		for(int x = 0; x < curMap.length; x++){
			curMap[0][x] = 1;
			curMap[curMap.length - 1][x] = 1;
		}
	}
	
	public static ArrayList<int[]> genPath(int x1, int y1, int x2, int y2){
		ArrayList<int[]> result = new ArrayList<int[]>();
		ArrayList<Integer> path = Game.astar.path(x1, y1, x2, y2);
		int x = x1;
		int y = y1;
		result.add(new int[]{x - 1, y, 24});
		int img = 18;
		int prev = 0;
		int cur = 0;
		for(int n = 0; n < path.size(); n++){
			cur = path.get(n);
			if(prev == 1){
				if(cur == 1){
					x++;
				}
				else if(cur == 2){
					y++;
				}
				else if(cur == 4){
					y--;
				}
			}
			else if(prev == 2){
				if(cur == 1){
					x++;
				}
				else if(cur == 2){
					y++;
				}
				else if(cur == 3){
					x--;
				}
			}
			else if(prev == 3){
				if(cur == 2){
					y++;
				}
				else if(cur == 3){
					x--;
				}
				else if(cur == 4){
					y--;
				}
			}
			else if(prev == 4){
				if(cur == 1){
					x++;
				}
				else if(cur == 3){
					x--;
				}
				else if(cur == 4){
					y--;
				}
			}
			else{
				if(cur == 1){
					x++;
				}
				else if(cur == 2){
					y++;
				}
				else if(cur == 3){
					x--;
				}
				else if(cur == 4){
					y--;
				}
			}
			result.add(new int[]{x, y, img});
			prev = cur;
		}
		result.add(new int[]{x2 + 1, y2, 24});
		int xb;
		int yb;
		int xf;
		int yf;
		for(int n = 1; n < result.size() - 1; n++){
			xb = result.get(n - 1)[0] - result.get(n)[0];
			yb = result.get(n - 1)[1] - result.get(n)[1];
			xf = result.get(n)[0] - result.get(n + 1)[0];
			yf = result.get(n)[1] - result.get(n + 1)[1];
			
			if(xb == 1){
				if(xf == 1){
					img = 19;
				}
				else if(xf == -1){
					img = 19;
				}
				else if(yf == 1){
					img = 23;
				}
				else{
					img = 22;
				}
			}
			else if(xb == -1){
				if(xf == 1){
					img = 19;
				}
				else if(xf == -1){
					img = 19;
				}
				else if(yf == 1){
					img = 21;
				}
				else{
					img = 20;
				}
			}
			else if(yb == 1){
				if(xf == 1){
					img = 20;
				}
				else if(xf == -1){
					img = 22;
				}
				else if(yf == 1){
					img = 18;
				}
				else{
					img = 18;
				}
			}
			else if(yb == -1){
				if(xf == 1){
					img = 21;
				}
				else if(xf == -1){
					img = 23;
				}
				else if(yf == 1){
					img = 18;
				}
				else{
					img = 18;
				}
			}
			
			result.get(n)[2] = img;
		}
		return result;
	}
}
