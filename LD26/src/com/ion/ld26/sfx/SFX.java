package com.ion.ld26.sfx;

import java.applet.AudioClip;

public class SFX {
	public static boolean mute = false;
	public static AudioClip portal;
	public static AudioClip keyPickup;
	public static AudioClip punch;
	public static AudioClip teleport;
	public static AudioClip hurt;
	public static AudioClip explosion;
	public static AudioClip music;
}
