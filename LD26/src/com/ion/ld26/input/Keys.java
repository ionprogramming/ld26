package com.ion.ld26.input;

import java.awt.event.KeyEvent;

import com.ion.ld26.Game;
import com.ion.ld26.map.Map;
import com.ion.ld26.sfx.SFX;

public class Keys {
	
	public static boolean up = false;
	public static boolean down = false;
	public static boolean left = false;
	public static boolean right = false;
	public static boolean canPunch = true;
	
	public static void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_W){
			up = true;
		}
		if(e.getKeyCode() == KeyEvent.VK_S){
			down = true;
		}
		if(e.getKeyCode() == KeyEvent.VK_A){
			left = true;
		}
		if(e.getKeyCode() == KeyEvent.VK_D){
			right = true;
		}
		if(e.getKeyCode() == KeyEvent.VK_SPACE && canPunch){
			Game.entities.get(0).punch();
			canPunch = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER && Map.ranRange == 8){
			Map.ranRange = 0;
			Map.initStage(0);
		}
		
		
	}


	public static void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_W){
			up = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_S){
			down = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_A){
			left = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_D){
			right = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_SPACE){
			canPunch = true;
		}
		if(e.getKeyCode() == KeyEvent.VK_M){
			if(SFX.mute){
				SFX.mute = false;
				SFX.music.stop();
			}
			else{
				SFX.mute = true;
				SFX.music.loop();
			}
		}
	}


	public static void keyTyped(KeyEvent e) {
		
	}
}
